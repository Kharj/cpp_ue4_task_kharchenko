//Evgen Kharchenko cpp task
#include <iostream>
#include <string>
#include <limits>

int ParseInt(const char* string)
{
	bool isNegative = false;
	int result = 0;
	const int maxVal = std::numeric_limits<int>::max();
	while (*string != '\0')
	{
		if (*string >= '0' && *string <= '9')
		{
			//Type overflow check
			if (maxVal / 10 > result ||
				(maxVal / 10 == result &&
					maxVal % 10 >= (*string - '0')))
			{
				result *= 10;
				result += (*string - '0');
			}
		}
		else if (*string == '-' && result == 0)
		{
			isNegative = true;
		}
		string++;
	}

	if (isNegative)
	{
		result *= -1;
	}
	return result;
}


int main(int argc, char* argv[])
{
	/*std::cout << "Input number: ";
	std::string inputString;
	std::getline(std::cin, inputString);

	int parsedInt = ParseInt(inputString.data());
	std::cout << "\nParsed int: " << parsedInt;
	*/
	std::cout << ParseInt("abc") << std::endl;
	std::cout << ParseInt("153") << std::endl;
	std::cout << ParseInt("-153") << std::endl;
	std::cout << ParseInt("...-..15.3..") << std::endl;
	std::cout << ParseInt("2147483647") << std::endl;
	std::cout << ParseInt("2147483648") << std::endl;
	std::cout << ParseInt("-2147483647") << std::endl;
	std::cout << ParseInt("-2147483648") << std::endl;
	std::cout << "Any key to exit\n";
	std::cin.get();
	return 0;
}
