//Evgen Kharchenko cpp task
#include <iostream>
#include <string>
#include <cassert>


void SpiralPrint(const int* inputArray, const int sizeX, const int sizeY)
{
	struct Vec
	{
		int x{ 0 }, y{ 0 };
	};

	assert(sizeX > 0 && sizeY > 0);
	//Bounds of unprocessed area
	Vec boundsMin{ 0, 0 }, boundsMax{ sizeX - 1, sizeY - 1 };
	Vec direction{ 1, 0 };
	Vec currentPos{ boundsMin };

	while (boundsMin.x <= boundsMax.x && boundsMin.y <= boundsMax.y)
	{
		assert(!direction.x || !direction.y);

		const Vec& targetBound = (direction.x + direction.y > 0) ? boundsMax : boundsMin;

		const int distanceToBound{ abs((targetBound.x - currentPos.x) * direction.x) +
									abs((targetBound.y - currentPos.y) * direction.y) };

		for (int i = 0; i < distanceToBound; ++i)
		{
			assert(currentPos.x >= 0 && currentPos.y >= 0);
			assert(currentPos.x < sizeX && currentPos.y < sizeY);
			std::cout << inputArray[currentPos.y * sizeX + currentPos.x] << " ";

			currentPos.x += direction.x;
			currentPos.y += direction.y;
		}

		//Rotate direction 90deg clockwise
		std::swap(direction.x, direction.y);
		direction.x *= -1;

		//Update bounds
		Vec& oppositeBound = (direction.x + direction.y < 0) ? boundsMax : boundsMin;
		oppositeBound.x += direction.x;
		oppositeBound.y += direction.y;
	}

	//Print last one
	assert(currentPos.x >= 0 && currentPos.y >= 0);
	assert(currentPos.x < sizeX && currentPos.y < sizeY);
	std::cout << inputArray[currentPos.y * sizeX + currentPos.x] << " ";
}
int main(int argc, char* argv[])
{
	/*
	const size_t sizeX = 4;
	const size_t sizeY = 4;
	const int inputArray[sizeX * sizeY]{
		1, 2, 3, 4,
		12, 13, 14, 5,
		11, 16, 15, 6,
		10, 9, 8, 7
	};
	*/
	/*
	const size_t sizeX = 1;
	const size_t sizeY = 4;
	const int inputArray[sizeX * sizeY]{
		1,
		2,
		3,
		4
	};
	*/
	/*
	const size_t sizeX = 2;
	const size_t sizeY = 4;
	const int inputArray[sizeX * sizeY]{
		1, 2,
		3, 4,
		5, 6,
		7, 8
	};
	*/
	/*
	const size_t sizeX = 3;
	const size_t sizeY = 4;
	const int inputArray[sizeX * sizeY]{
		1, 2, 3,
		4, 5, 6,
		7, 8, 9,
		10, 11, 12
	};
	*/
	/*
	const size_t sizeX = 6;
	const size_t sizeY = 1;
	const int inputArray[sizeX * sizeY]{
		1, 2, 3, 4, 5, 6 };
	*/

	const size_t sizeX = 3;
	const size_t sizeY = 3;
	const int inputArray[sizeX * sizeY]{
		1, 2, 3,
		4, 5, 6,
		7, 8, 9
	};

	SpiralPrint(inputArray, sizeX, sizeY);

	std::cout << "\nAny key to exit\n";
	std::cin.get();
	return 0;
}
