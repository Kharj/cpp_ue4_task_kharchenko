// Fill out your copyright notice in the Description page of Project Settings.
// Evgen Kharchenko Task

#include "TextWithBgActor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/TextRenderComponent.h"
#include "Math/Vector.h"

ATextWithBgActor::ATextWithBgActor()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ATextWithBgActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ATextWithBgActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UStaticMeshComponent* StaticMeshComponent = Cast<UStaticMeshComponent>(AActor::FindComponentByClass(UStaticMeshComponent::StaticClass()));
	const UTextRenderComponent* TextRenderComponent = Cast<UTextRenderComponent>(AActor::FindComponentByClass(UTextRenderComponent::StaticClass()));

	if (!StaticMeshComponent || !TextRenderComponent)
	{
		return;
	}

	//Mesh props
	FVector MeshBoundsMin, MeshBoundsMax;
	StaticMeshComponent->GetLocalBounds(MeshBoundsMin, MeshBoundsMax);
	const FRotator MeshRotation = StaticMeshComponent->GetRelativeRotation();

	//Apply local transforms, so we work in Actor coords
	FVector MeshSizeActorCoords = MeshRotation.RotateVector(MeshBoundsMax - MeshBoundsMin);
	MeshSizeActorCoords.X = 1.0f;//Prevent div by zero

	FVector TextLocalSize = TextRenderComponent->GetTextLocalSize();
	TextLocalSize.X = 1.0f;//Prevent zero Z-scale

	const FVector MarginScaleActorCoords(0.0f, MarginLeft + MarginRight, MarginTop + MarginBottom);
	const FVector MarginLocationActorCoords(FVector(0.0f, MarginLeft - MarginRight, MarginTop - MarginBottom) * 0.5f);

	//Convert scale back to Mesh coords
	const FVector MeshScaleActorCoords = (TextLocalSize + MarginScaleActorCoords) / MeshSizeActorCoords;
	const FVector MeshScaleMeshCoords = MeshRotation.GetInverse().RotateVector(MeshScaleActorCoords);

	//Clamp scale to non-zero
	const FVector MinMeshScale(0.01f, 0.01f, 0.01f);
	FVector MeshScaleClamped(
		FMath::Max(MinMeshScale.X, MeshScaleMeshCoords.X),
		FMath::Max(MinMeshScale.Y, MeshScaleMeshCoords.Y),
		FMath::Max(MinMeshScale.Z, MeshScaleMeshCoords.Z)
	);

	StaticMeshComponent->SetRelativeScale3D(MeshScaleClamped);
	StaticMeshComponent->SetRelativeLocation_Direct(MarginLocationActorCoords);
}

