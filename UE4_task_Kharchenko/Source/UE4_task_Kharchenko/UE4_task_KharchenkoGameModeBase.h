// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UE4_task_KharchenkoGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UE4_TASK_KHARCHENKO_API AUE4_task_KharchenkoGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
