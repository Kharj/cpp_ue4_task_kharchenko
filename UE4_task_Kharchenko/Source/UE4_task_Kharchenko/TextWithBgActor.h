// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "TextWithBgActor.generated.h"

UCLASS()
class UE4_TASK_KHARCHENKO_API ATextWithBgActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ATextWithBgActor();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Margins)
		float MarginTop = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Margins)
		float MarginBottom = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Margins)
		float MarginLeft = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Margins)
		float MarginRight = 0.0f;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

};
