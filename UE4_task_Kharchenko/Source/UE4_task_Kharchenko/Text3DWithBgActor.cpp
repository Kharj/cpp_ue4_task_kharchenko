// Fill out your copyright notice in the Description page of Project Settings.
#include "Text3DWithBgActor.h"
#include "Text3DComponent.h"

AText3DWithBgActor::AText3DWithBgActor()
{
	Text3DComponent = CreateDefaultSubobject<UText3DComponent>(TEXT("Text3DComponent"));
	RootComponent = Text3DComponent;
	PrimaryActorTick.bCanEverTick = true;
}

void AText3DWithBgActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UStaticMeshComponent* StaticMeshComponent = Cast<UStaticMeshComponent>(AActor::FindComponentByClass(UStaticMeshComponent::StaticClass()));
	if (!StaticMeshComponent)
	{
		return;
	}

	//Mesh props
	FVector MeshBoundsMin, MeshBoundsMax;
	StaticMeshComponent->GetLocalBounds(MeshBoundsMin, MeshBoundsMax);
	const FRotator MeshRotation = StaticMeshComponent->GetRelativeRotation();

	//Apply local transforms, so we work in Actor coords
	FVector MeshSizeActorCoords = MeshRotation.RotateVector(MeshBoundsMax - MeshBoundsMin);
	MeshSizeActorCoords.X = 1.0f;//Prevent div by zero

	//Calc bounding box of all glyphs
	FBoxSphereBounds GlyphsTotalBounds;

	const int32 GlyphsCount = Text3DComponent->GetGlyphCount();
	for (int32 Index = 0; Index < GlyphsCount; Index++)
	{
		const UStaticMeshComponent* GlyphMesh = Text3DComponent->GetGlyphMeshComponent(Index);
		const USceneComponent* KerningMesh = Text3DComponent->GetGlyphKerningComponent(Index);
		if (Index == 0)
		{
			//Init Bounds with first glyph to do not combine with its default value
			GlyphsTotalBounds = GlyphMesh->Bounds;
		}
		GlyphsTotalBounds = GlyphsTotalBounds + GlyphMesh->Bounds + KerningMesh->Bounds;
	}

	FVector TextLocalSize = GlyphsTotalBounds.BoxExtent * 2.0f;

	const FVector MarginScaleActorCoords(0.0f, MarginLeft + MarginRight, MarginTop + MarginBottom);
	const FVector MarginLocationActorCoords(FVector(0.0f, MarginLeft - MarginRight, MarginTop - MarginBottom) * 0.5f +
		FVector(TextLocalSize.X, 0.0f, 0.0f));//Shift plane behind 3D Text

	//Convert scale back to Mesh coords
	TextLocalSize.X = 1.0f;//Prevent zero Z-scale
	const FVector MeshScaleActorCoords = (TextLocalSize + MarginScaleActorCoords) / MeshSizeActorCoords;
	const FVector MeshScaleMeshCoords = MeshRotation.GetInverse().RotateVector(MeshScaleActorCoords);

	//Clamp scale to non-zero
	const FVector MinMeshScale(0.01f, 0.01f, 0.01f);
	FVector MeshScaleClamped(
		FMath::Max(MinMeshScale.X, MeshScaleMeshCoords.X),
		FMath::Max(MinMeshScale.Y, MeshScaleMeshCoords.Y),
		FMath::Max(MinMeshScale.Z, MeshScaleMeshCoords.Z)
	);

	StaticMeshComponent->SetRelativeScale3D(MeshScaleClamped);
	StaticMeshComponent->SetRelativeLocation_Direct(MarginLocationActorCoords);
}
