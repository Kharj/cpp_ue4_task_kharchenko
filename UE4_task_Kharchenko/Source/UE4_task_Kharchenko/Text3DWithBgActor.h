// Kharchenko Evgen test task
// Partially copied from AText3DActor since can not derive 'final' class

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Text3DWithBgActor.generated.h"


UCLASS(Meta = (ComponentWrapperClass))
class UE4_TASK_KHARCHENKO_API AText3DWithBgActor : public AActor
{
	GENERATED_BODY()

public:
	AText3DWithBgActor();
	class UText3DComponent* GetText3DComponent() const { return Text3DComponent; }

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Margins)
		float MarginTop = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Margins)
		float MarginBottom = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Margins)
		float MarginLeft = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Margins)
		float MarginRight = 0.0f;

private:
	UPROPERTY(Category = Text, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UText3DComponent* Text3DComponent;

	virtual void Tick(float DeltaTime) override;
};
